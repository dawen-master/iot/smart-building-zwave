import getopt
import sys

import requests

base_url = 'http://192.168.1.2:5000/'


#######################################################################################################################
########################################## NETWORK FUNCTIONS  #########################################################
#######################################################################################################################

def get_network_info(nid, parameter, content):
    print "Network info"
    print requests.get(base_url + "network/info").content


#######################################################################################################################
########################################## NODES FUNCTIONS  ###########################################################
#######################################################################################################################


def get_node_neighbors(nid, parameter, content):
    if nid is None:
        print "Missing node id"
        return

    print "Neighbors for node {}".format(nid)
    print requests.get(base_url + "nodes/{}/get_neighbours".format(nid)).content


def get_node_location(nid, parameter, content):
    if nid is None:
        print "Missing node id"
        return

    print "Location of node {}".format(nid)
    print requests.get(base_url + "nodes/{}/get_location".format(nid)).content


def set_node_location(nid, parameter, content):
    if nid is None:
        print "Missing node id"
        return

    if content is None:
        print "Missing content"
        return

    print "Set location of node {} to {}".format(nid, content)
    print requests.post(base_url + "nodes/set_location", None, {"node_id": nid, "value": content}).content


#######################################################################################################################
########################################## SENSORS FUNCTIONS  #########################################################
#######################################################################################################################

def get_sensors_list(nid, parameter, content):
    print "Get sensors list"
    print requests.get(base_url + "sensors/get_sensors_list").content


def get_sensors_humidity(nid, parameter, content):
    if nid is None:
        print "Missing sensor id"
        return

    print "Neighbors for node {}".format(nid)
    print requests.get(base_url + "sensors/{}/get_humidity".format(nid)).content


# GET	/sensors/{$sensor_id}/get_all_measures	Return all measures (temp, hum, lum, motion ...) for sensor $sensor_id
# GET	/sensors/{$sensor_id}/get_temperature	Return current temperature for sensor $sensor_id
# GET	/sensors/{$sensor_id}/get_luminance	Return current luminance for sensor $sensor_id
# GET	/sensors/{$sensor_id}/get_motion

operations = {
    1: get_network_info,
    2: get_node_neighbors,
    3: get_node_location,
    4: set_node_location,
    5: get_sensors_list,
    6: get_sensors_humidity,
}


def main(argv):
    # parse args
    try:
        opts, args = getopt.getopt(argv, "hq:i:p:c:", ["query", "id", "parameter", "content"])
    except getopt.GetoptError:
        print_help()
        sys.exit(2)

    query = None
    nid = None
    parameter = None
    content = None

    for opt, arg in opts:
        if opt == '-h':
            print_help()
            exit(1)
        elif opt in ('-q', '--query'):
            query = int(arg)
        elif opt in ('-i', '--id'):
            nid = arg
        elif opt in ('-p', '--parameter'):
            parameter = arg
        elif opt in ('-c', '--content'):
            content = arg

    try:
        operations[query](nid, parameter, content)
    except KeyError as e:
        print "operation {} not exist".format(query)


def print_help():
    print "Usage: python client.py -q <query> -i <node, sensor or dimmer> -p <parameter> -c <post_content>\n" \
          "Options:\n" \
          "1: get network info\n" \
          "2: get_node_neighbors\n" \
          "3: get_node_location\n" \
          "4: set_node_location"


if __name__ == '__main__':
    main(sys.argv[1:])
